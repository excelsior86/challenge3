# Challenge3: solution

## Intro
The project is made up of different modules. 
The aim is to build a **CI/CD** system using **codecommit**. The original code was modified with the addition of a **Dockerfile**. Using **codebuild** a new image of the application is built and pushed on **ECR**

## The two stacks
Both solutions use docker. The first stack uses **Fargate** and **ALB**. The second one **Elastick Beanstalk**

## Restricting access
The less complex way I found to implement access restriction is implementing Autherization Grant using Amazon **Cognito**. Due to semplicity, cognito was implemented only for the ecs stack

## Notes
| Tool       | Version      |
| :--------: | :----------: |
| Terraform  | 0.14         |
| AWS Region | eu-central-1 |



