variable "project_name" {
  type = string
}
variable "certificate_arn" {
  type = string
}

variable "user_pool_domain" {
  type = string
}