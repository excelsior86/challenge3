resource "aws_security_group" "alb" {
  name = "${var.project_name}-sg-alb"
  ingress = [{
    cidr_blocks      = ["0.0.0.0/0"]
    description      = "incoming web traffic"
    from_port        = 80
    ipv6_cidr_blocks = []
    prefix_list_ids  = []
    protocol         = "tcp"
    security_groups  = []
    self             = true
    to_port          = 80    
  },
  {
    cidr_blocks = ["0.0.0.0/0"]
    description = "incoming https traffic"
    from_port = 443
    ipv6_cidr_blocks = []
    prefix_list_ids = []
    protocol = "tcp"
    security_groups = []
    self = true 
    to_port = 443
  }]
  egress = [{
    cidr_blocks      = ["0.0.0.0/0"]
    description      = "all traffic outgoing"
    from_port        = 0
    ipv6_cidr_blocks = []
    prefix_list_ids  = []
    protocol         = "-1"
    security_groups  = []
    self             = false
    to_port          = 0
  }]
  tags = {
    "Name"    = "${var.project_name}-alb"
    "Project" = var.project_name
  }
}

resource "aws_lb" "alb" {
  name = var.project_name
  internal = false
  load_balancer_type = "application"
  security_groups = [ aws_security_group.alb.id ]
  subnets = [ data.aws_subnet.sub-a.id, data.aws_subnet.sub-b.id, data.aws_subnet.sub-c.id]
  tags = {
    "Project" = var.project_name
  }
}

resource "aws_lb_listener" "alb" {
  load_balancer_arn = aws_lb.alb.arn
  port="80"
  protocol = "HTTP"  
  default_action {
    type = "redirect"
    redirect {
      port = 443
      protocol = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_listener" "alb_https" {
  load_balancer_arn = aws_lb.alb.arn
  port="443"
  protocol = "HTTPS"  
  certificate_arn = var.certificate_arn
  default_action {
      type = "authenticate-cognito"
      authenticate_cognito {
        user_pool_arn = aws_cognito_user_pool.user_pool.arn
        user_pool_client_id = aws_cognito_user_pool_client.client.id
        user_pool_domain = aws_cognito_user_pool_domain.user_pool_domain.domain
      }
  } 
  default_action {
    type = "forward"
    target_group_arn = aws_lb_target_group.challenge.arn
  }
}

resource "aws_lb_target_group" "challenge" {
  name = var.project_name
  port=80
  protocol = "HTTP"
  vpc_id = aws_default_vpc.default.id
  stickiness {
    type = "lb_cookie"
  }
  health_check {
    enabled = true
    interval = 60
    path = "/"
    port = 80
    protocol="HTTP"
    timeout = 30
    healthy_threshold = 3
    unhealthy_threshold = 5
    matcher = "200-302"
  }
  target_type = "ip"
  depends_on = [ aws_lb.alb ]
  tags = {
    "Project" = var.project_name
  }
}

resource "aws_default_vpc" "default" {}

data "aws_subnet" "sub-a" {
  vpc_id = aws_default_vpc.default.id
  availability_zone = "eu-central-1a"
}

data "aws_subnet" "sub-b" {
  vpc_id = aws_default_vpc.default.id
  availability_zone = "eu-central-1b"
}

data "aws_subnet" "sub-c" {
  vpc_id = aws_default_vpc.default.id
  availability_zone = "eu-central-1c"
}


resource "aws_cognito_user_pool" "user_pool" {
  name = "user_pool"
 
}

resource "aws_cognito_user_pool_client" "client" {
  name = "pool_client"
  user_pool_id = aws_cognito_user_pool.user_pool.id
  generate_secret = true
  allowed_oauth_flows = ["code"]
  allowed_oauth_scopes = ["openid"]
  callback_urls = ["https://${aws_lb.alb.dns_name}/oauth2/idpresponse"]
  logout_urls = [ "https://${aws_lb.alb.dns_name}/oauth2/logout" ]
  explicit_auth_flows = ["ALLOW_REFRESH_TOKEN_AUTH"]  
  supported_identity_providers = [ "COGNITO" ]  
  allowed_oauth_flows_user_pool_client = true
  prevent_user_existence_errors = "ENABLED"
}

resource "aws_cognito_user_pool_domain" "user_pool_domain" {
  domain       = var.user_pool_domain
  user_pool_id = aws_cognito_user_pool.user_pool.id
}
