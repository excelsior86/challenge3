###CI/CD 

module "ci_cd" {
  source = "./ci_cd"
  coderepo_name = "test"
  project_name="challenge3"
  env = "test"
}


module "users" {
    source = "./users"
    coderepo_arn=[module.ci_cd.coderepo_arn]    
    dev_users=["test_user", "test_user2"]    
}

###First solution using ECS Fargate

# module "cluster" {
#   source = "./ecs"
#   docker_image = module.ci_cd.docker_registry
#   container_definition_file = "containerdefinition.json"
#   project_name = "challenge"
#   target_group_arn = module.alb.target_group_arn
# }

# module "alb" {
#   source = "./alb"
#   project_name = "challenge"
#   certificate_arn = module.cert.certificate_arn
#   user_pool_domain = "challenge-test"
# }


# ##Security for first solution
# module "cert" {
#   source = "./iam_cert"
# }

# module "cognito" {
#   source = "./cognito"
#   callback_base_url = "https://challenge-1697081221.eu-central-1.elb.amazonaws.com"
#   user_pool_domain = "challenge-test"
# }


###Second solution using Beanstalk

# module "beanstalk" {
#   source = "./beanstalk"  
# }