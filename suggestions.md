# Suggestions

Given the fact that updates appears very infrequent, maybe a better approach would be to generate static pages to upload on an **s3 bucket** and serve them from **cloudfront**. This way we can avoid to use computational capacity on both building the ecr image and then to execute ecs tasks in the first scenario, ec2 instances in the beanstalk one, saving a lot of money