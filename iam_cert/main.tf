resource "aws_iam_server_certificate" "cert" {
  name             = "cert"
  certificate_body = file("server.pem")
  private_key      = file("pk.pem")
}