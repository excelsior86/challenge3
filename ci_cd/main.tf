resource "aws_codecommit_repository" "coderepo" {
  repository_name = var.coderepo_name
  tags = {
      "Name" = var.coderepo_name
      "Project" = var.project_name
      "Environment" = var.env
  }
}


resource "aws_ecr_repository" "dockerimage" {
  name = var.project_name
}


data "aws_iam_policy_document" "codebuilder_assume_role" {
  statement {
    effect = "Allow"
    actions = [ "sts:AssumeRole" ]
    principals {      
      type = "Service"
      identifiers = [ "codebuild.amazonaws.com" ]
    }
  }
}

data "aws_iam_policy_document" "codebuilder_policy" {
    statement {
      effect = "Allow"
      actions = ["codecommit:GitPull"]
      resources = [ aws_codecommit_repository.coderepo.arn ]
    }
    statement {
      effect = "Allow"
      actions= [
          "ecr:BatchGetImage",
          "ecr:BatchCheckLayerAvailability",
          "ecr:CompleteLayerUpload",
          "ecr:DescribeImages",
          "ecr:DescribeRepositories",
          "ecr:GetDownloadUrlForLayer",
          "ecr:InitiateLayerUpload",
          "ecr:ListImages",
          "ecr:PutImage",
          "ecr:UploadLayerPart"
        ]
      resources = [ aws_ecr_repository.dockerimage.arn ]
    }    
    statement {
      effect = "Allow"
      actions=[
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ]
      resources = ["*"]
    }
    statement {
        effect = "Allow"
        actions = [ "ecr:GetAuthorizationToken" ]
        resources = [ "*" ]
    }   
}

resource "aws_iam_role" "codebuild" {
  name = "codebuild"
  assume_role_policy = data.aws_iam_policy_document.codebuilder_assume_role.json
  inline_policy {
    name = "codebuilder_policy"
    policy = data.aws_iam_policy_document.codebuilder_policy.json
  }
}


resource "aws_codebuild_project" "challenge3" {
  name = "challenge3"
  service_role = aws_iam_role.codebuild.arn
  artifacts {
    type = "NO_ARTIFACTS"
  }
  environment {
    compute_type                = "BUILD_GENERAL1_SMALL"
    image                       = "aws/codebuild/amazonlinux2-x86_64-standard:3.0"
    type                        = "LINUX_CONTAINER"
    image_pull_credentials_type = "CODEBUILD"
    privileged_mode = true
    environment_variable {
      name = "ECR"
      value = aws_ecr_repository.dockerimage.repository_url
    }
  }
  logs_config {
    cloudwatch_logs {
      group_name  = var.project_name
      stream_name = var.project_name
    }
  }
  source {
      type = "CODECOMMIT"
      location = aws_codecommit_repository.coderepo.clone_url_http        
      
  }  
  source_version = "refs/heads/master"  
}