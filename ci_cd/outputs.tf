output "coderepo_ssh" {
  value = aws_codecommit_repository.coderepo.clone_url_ssh
}
output "coderepo_arn" {
  value = aws_codecommit_repository.coderepo.arn
}

output "docker_registry" {
  value = aws_ecr_repository.dockerimage.repository_url
}