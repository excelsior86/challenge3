variable "project_name" {
  type = string
  default = "challenge"
}

variable "docker_image" {
  type = string
}

variable "container_definition_file" {
 type = string 
}

variable "target_group_arn" {
  type = string
}