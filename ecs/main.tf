data "aws_iam_policy_document" "ecs_tasks_execution_assume_role" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "log_permissions" {
  statement {
      effect = "Allow"
      actions=[
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ]
      resources = ["*"]
    }
}


resource "aws_iam_role" "challenge_ecs_task" {
  name="challengeEcsTaskRole"  
  assume_role_policy = data.aws_iam_policy_document.ecs_tasks_execution_assume_role.json
  inline_policy {
     name = "log_permissions"
     policy = data.aws_iam_policy_document.log_permissions.json 
  }
}

resource "aws_iam_role_policy_attachment" "challenge-ecs-task-attachment" {
  role = aws_iam_role.challenge_ecs_task.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}


resource "aws_ecs_cluster" "challenge-cluster" {
  name = "${var.project_name}-cluster"
  capacity_providers = [ "FARGATE" ]
  tags = {
    "Name" = "${var.project_name}-cluster"
    "Project" = var.project_name
  }
}


resource "aws_ecs_task_definition" "challenge_task" {
  family = var.project_name
  container_definitions = file(var.container_definition_file)
  requires_compatibilities = [ "FARGATE" ]  
  cpu = 256
  memory = 1024
  network_mode = "awsvpc"
  execution_role_arn = aws_iam_role.challenge_ecs_task.arn  
  tags = {    
    "Project" = var.project_name
  }
}

resource "aws_ecs_service" "service" {
  name = "challenge"
  cluster = aws_ecs_cluster.challenge-cluster.arn
  desired_count = 1 
  launch_type = "FARGATE"
  platform_version = "1.4.0"
  enable_ecs_managed_tags = true
  propagate_tags = "TASK_DEFINITION"
  force_new_deployment = false
  task_definition = "${aws_ecs_task_definition.challenge_task.family}:${aws_ecs_task_definition.challenge_task.revision}"
  network_configuration {
    subnets = [ data.aws_subnet.sub-a.id,  data.aws_subnet.sub-b.id, data.aws_subnet.sub-c.id ]
    security_groups = [ aws_security_group.container_group.id ]
    assign_public_ip = true
  }
  health_check_grace_period_seconds = 120
  load_balancer {
    target_group_arn = var.target_group_arn
    container_port = 3000
    container_name = "challenge"
  }
}


resource "aws_security_group" "container_group" {
  name = "${var.project_name}-sg-container"
  ingress = [{
    cidr_blocks      = [aws_default_vpc.default.cidr_block]
    description      = "incoming traffic"
    from_port        = 3000
    ipv6_cidr_blocks = []
    prefix_list_ids  = []
    protocol         = "tcp"
    security_groups  = []
    self             = true
    to_port          = 3000 
  }
  ]
  egress = [{
    cidr_blocks      = ["0.0.0.0/0"]
    description      = "all traffic outgoing"
    from_port        = 0
    ipv6_cidr_blocks = []
    prefix_list_ids  = []
    protocol         = "-1"
    security_groups  = []
    self             = false
    to_port          = 0
  }]
  tags = {
    "Name"    = "${var.project_name}-group"
    "Project" = var.project_name
  }
}


resource "aws_default_vpc" "default" {}

data "aws_subnet" "sub-a" {
  vpc_id = aws_default_vpc.default.id
  availability_zone = "eu-central-1a"
}

data "aws_subnet" "sub-b" {
  vpc_id = aws_default_vpc.default.id
  availability_zone = "eu-central-1b"
}

data "aws_subnet" "sub-c" {
  vpc_id = aws_default_vpc.default.id
  availability_zone = "eu-central-1c"
}


resource "aws_cloudwatch_log_group" "logs" {
  name = "/ecs/challenge"
  tags = {
    "Project" = var.project_name
  }
}








