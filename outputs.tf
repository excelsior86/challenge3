output "coderepo_ssh" {
  value = module.ci_cd.coderepo_ssh
}

output "coderepo_users" {
  value = module.users.coderepo_users
}

# output "app" {
#   value = module.alb.lb_url
# }

# output "endpoint_url" {
#   value = module.beanstalk.endpoint_url
# }