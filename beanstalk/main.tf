locals {
  nsvpc="aws:ec2:vpc"
}

resource "aws_elastic_beanstalk_application" "challenge3" {
  name = "challenge3"
}
resource "aws_elastic_beanstalk_environment" "challenge3" {
  name = "challeng3-env"
  application = aws_elastic_beanstalk_application.challenge3.name
  tier = "WebServer"
  solution_stack_name = data.aws_elastic_beanstalk_solution_stack.sol_stack.name
  setting {
    namespace = local.nsvpc
    name = "VPCId"
    value = aws_default_vpc.default.id
  }
  setting {
      namespace = local.nsvpc
      name = "Subnets"
      value = join(",", [data.aws_subnet.sub-a.id,data.aws_subnet.sub-b.id,data.aws_subnet.sub-c.id])
  }
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name = "IamInstanceProfile"
    value = aws_iam_instance_profile.instanceprofile.arn
  }
  setting {
    namespace = "aws:elasticbeanstalk:environment"
    name = "ServiceRole"
    value = aws_iam_role.servicerole.arn
  }
}

data "aws_elastic_beanstalk_solution_stack" "sol_stack" {
  most_recent = true
  name_regex = "^64bit Amazon Linux 2 v3.2.6 running Docker$"
}


resource "aws_default_vpc" "default" {}

data "aws_subnet" "sub-a" {
  vpc_id = aws_default_vpc.default.id
  availability_zone = "eu-central-1a"
}

data "aws_subnet" "sub-b" {
  vpc_id = aws_default_vpc.default.id
  availability_zone = "eu-central-1b"
}

data "aws_subnet" "sub-c" {
  vpc_id = aws_default_vpc.default.id
  availability_zone = "eu-central-1c"
}

resource "aws_iam_instance_profile" "instanceprofile" {
  name = "bsprofile"
  role = aws_iam_role.role.id
}

resource "aws_iam_role" "role" {
  assume_role_policy = data.aws_iam_policy_document.assumerole.json  
  # inline_policy {
  #   policy = data.aws_iam_policy_document.read_ecr.json
  # }
}

resource "aws_iam_role" "servicerole" {
  assume_role_policy = data.aws_iam_policy_document.ebassumerole.json  
  inline_policy {
    policy = data.aws_iam_policy_document.read_ecr.json
  }
}

resource "aws_iam_role_policy_attachment" "srbs" {
  role = aws_iam_role.servicerole.id
  policy_arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkManagedUpdatesCustomerRolePolicy"
}

resource "aws_iam_role_policy_attachment" "bs" {
    role = aws_iam_role.role.id
  policy_arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkWebTier"
}

resource "aws_iam_role_policy" "ecr_read_role" {
  role = aws_iam_role.role.id
  name = "ecr_read_role"
  policy = data.aws_iam_policy_document.read_ecr.json
}

data "aws_iam_policy_document" "read_ecr" {
  statement {
    effect = "Allow"
    actions = ["ecr:*"]
    resources = [ "*" ]
  }
}


data "aws_iam_policy_document" "assumerole" {
    statement {
      effect = "Allow"
      actions = [ "sts:AssumeRole" ]
      principals {
          type = "Service"
        identifiers = [ "ec2.amazonaws.com" ]
      }
    }
}

data "aws_iam_policy_document" "ebassumerole" {
    statement {
      effect = "Allow"
      actions = [ "sts:AssumeRole" ]
      principals {
          type = "Service"
        identifiers = [ "elasticbeanstalk.amazonaws.com" ]
      }
    }
}

resource "aws_elastic_beanstalk_application_version" "latest" {
  name = "latest"
  application = aws_elastic_beanstalk_application.challenge3.name
  bucket = "challenge3-dockerrun"
  key = "Dockerrun.aws.json"
}




