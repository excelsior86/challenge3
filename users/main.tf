resource "aws_iam_group" "dev" {
  name = "developers"  
}


data "aws_iam_policy_document" "dev" {
    statement {
      effect= "Allow"
      actions= [ "codecommit:*" ]
      resources=var.coderepo_arn
    }
}

resource "aws_iam_policy" "dev" {
    name = "dev_policy"
    policy = data.aws_iam_policy_document.dev.json    
}

resource "aws_iam_group_policy_attachment" "dev_policy_group_attachment" {
    group = aws_iam_group.dev.name
    policy_arn = aws_iam_policy.dev.arn
}

resource "aws_iam_user" "dev_users" {
    for_each = toset(var.dev_users)
    name=each.key
    
}

resource "aws_iam_user_ssh_key" "dev_user" {
    for_each = aws_iam_user.dev_users
    encoding = "SSH"
    public_key = file("${each.value.name}.pub")
    username = each.value.name
}

resource "aws_iam_group_membership" "dev_group_membership" {    
  name = "dev_group_membership"
  users = var.dev_users
  group = aws_iam_group.dev.name
  depends_on = [
    aws_iam_user.dev_users
  ]
}
   