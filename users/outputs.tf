output "coderepo_users" {
  value = {
      for user in aws_iam_user_ssh_key.dev_user:
        user.username => user.ssh_public_key_id
  }
}